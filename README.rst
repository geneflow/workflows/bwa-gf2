======================
BWA Alignment Workflow
======================

Version: v0.6

This is an example GeneFlow bioinformatics workflow with two steps: 1) Reference index creation with BWA Index, and 2) Sequence alignment with BWA Mem.

